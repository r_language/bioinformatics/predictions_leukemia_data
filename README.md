In this R file we created multiple classifiers with leukemia data from leukemiasEset library : 

- Multiclasspairs

- Random Forest

- Pamr

- Random Forest SRC


We used those classifiers to perform predictions on test data and we compared the different classifiers in the end (accuracy, kappa, top k genes, ...). You can also see the HTML output made with rmarkdown.
Have fun !

Author : Marion Estoup

Mail : marion_110@hotmail.fr

Date : July 2023